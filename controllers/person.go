package controllers

import (
	"strconv"
	"net/http"
	"../structs"
	"github.com/gin-gonic/gin"
	/*"crypto/hmac"*/
)

/*func CheckMAC(message, messageMAC, key []byte) bool {
	mac := hmac.New(sha256.New, key)
	mac.Write(message)
	expectedMAC := mac.Sum(nil)
	return hmac.Equal(messageMAC, expectedMAC)
}*/

func (idb *InDB) GetPerson(c *gin.Context) {
	var (
		person structs.Person
		result gin.H
	)
	id := c.Param("id")
	queryStmt, err := idb.DB.Prepare("SELECT Username,Password,First_Name,Last_Name,Telp,Age FROM people WHERE id=$1")
	err = queryStmt.QueryRow(id).Scan(&person.Username,&person.Password,&person.First_Name,&person.Last_Name,&person.Telp,&person.Age)

	if err != nil{
		// handle this error
		result = gin.H{
			"result": nil,
			"count":  0,
		}
	} else {
		result = gin.H{
			"result": person,
			"count":  1,
		}
	}

	c.JSON(http.StatusOK, result)
}

func (idb *InDB) GetPersons(c *gin.Context)  {
	var (
		persons []structs.Person
		result  gin.H
	)

	rows, err := idb.DB.Query(`SELECT username, password, first_name, last_name, telp, age FROM people`)

	if err != nil {
	// handle this error better than this
	panic(err)
	}

	defer rows.Close()

	for rows.Next(){
		var r structs.Person
    err = rows.Scan( &r.Username, &r.Password ,&r.First_Name, &r.Last_Name, &r.Telp, &r.Age)
    if err != nil{
      // handle this error
      panic(err)
		}
		persons= append(persons, r)
	}

	if len(persons) <= 0 {
		result = gin.H{
			"result": nil,
			"count":  0,
		}
	} else {
		result = gin.H{
			"result": persons,
			"count":  len(persons),
		}
	}


	c.JSON(http.StatusOK, result)
}

func (idb *InDB) CreatePerson(c *gin.Context) {
	var (
		person structs.Person
		result gin.H
	)

	first_name := c.PostForm("first_name")
	last_name := c.PostForm("last_name")
	telp := c.PostForm("telp")
	age, err := strconv.Atoi(c.PostForm("age"))
	username := c.PostForm("username")
	password := c.PostForm("password")
	if err != nil {
      // handle error
   }
	person.First_Name = first_name
	person.Last_Name = last_name
	person.Telp = telp
	person.Age = age
	person.Username = username
	person.Password = password
	idb.DB.Query(`INSERT INTO people (created_at, updated_at, first_name, last_name, telp, age, username, password)
VALUES (now(), now(), $1, $2, $3, $4, $5, $6)`, &person.First_Name, &person.Last_Name, &person.Telp, &person.Age, &person.Username, &person.Password)
	result = gin.H{
		"result": person,
	}
	c.JSON(http.StatusOK, result)
}

func (idb *InDB) UpdatePerson(c *gin.Context) {
	id := c.Query("id")
	first_name := c.PostForm("first_name")
	last_name := c.PostForm("last_name")
	telp := c.PostForm("telp")
	age, err2 := strconv.Atoi(c.PostForm("age"))
	username := c.PostForm("username")
	password := c.PostForm("password")
	if err2 != nil {
			// handle error
	 }
	var (
		//person    structs.Person
		newPerson structs.Person
		result    gin.H
	)

	_, err := idb.DB.Query(`SELECT name FROM people WHERE id = $1`, &id)
	if err != nil {
		result = gin.H{
			"result": "data not found",
		}
	}
	newPerson.First_Name = first_name
	newPerson.Last_Name = last_name
	newPerson.Telp = telp
	newPerson.Age = age
	newPerson.Username = username
	newPerson.Password = password
	_, err = idb.DB.Query(`UPDATE people SET updated_at=now(), first_name=$1, last_name=$2, telp=$3, age=$4, username=$5, password=$6 WHERE id=$7`, &newPerson.First_Name, &newPerson.Last_Name, &newPerson.Telp, &newPerson.Age,  &newPerson.Username, &newPerson.Password , &id)
	if err != nil {
		result = gin.H{
			"result": "update failed",
		}
	} else {
		result = gin.H{
			"result": "successfully updated data",
		}
	}
	c.JSON(http.StatusOK, result)
}

func (idb *InDB) DeletePerson(c *gin.Context) {
	var (
		//person structs.Person
		result gin.H
	)
	id := c.Param("id")
	_, err := idb.DB.Query(`SELECT name FROM people WHERE id = $1`, &id)
	if err != nil {
		result = gin.H{
			"result": "data not found",
		}
	}
	_, err = idb.DB.Query(`DELETE FROM people WHERE id = $1`, &id)
	if err != nil {
		result = gin.H{
			"result": "delete failed",
		}
	} else {
		result = gin.H{
			"result": "Data deleted successfully",
		}
	}

	c.JSON(http.StatusOK, result)
}
