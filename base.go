package main

import (
	"fmt"
	"net/http"
	"strings"
	"time"
	"database/sql"
	"io/ioutil"
	"./structs"
	"./config"
	"./controllers"
	jwt "github.com/dgrijalva/jwt-go"
	"github.com/gin-gonic/gin"
	_ "github.com/lib/pq"
)

var jwtSecret = "testhmac"

type Credential struct {
	Username string `json:"username"`
	Password string `json:"password"`
	Hmac_timestamp int `json:"hmac_timestamp"`
	Hmac_sign int `json:"hmac_sign"`
}
var PATH = "/v0"

var db *sql.DB

func main() {
	//gin.SetMode(gin.ReleaseMode)
	gin.DefaultWriter = ioutil.Discard

	db = config.DBInit()
	inDB := &controllers.InDB{DB: db}

	router := gin.Default()

	router.POST(PATH+"/login", loginHandler)
	router.PUT(PATH+"/token", refreshToken)
	router.GET(PATH+"/token", viewToken)
	router.GET(PATH+"/person/:id", auth, inDB.GetPerson)
	router.GET(PATH+"/persons", auth, inDB.GetPersons)
	router.POST(PATH+"/person", auth, inDB.CreatePerson)
	router.PUT(PATH+"/person", auth, inDB.UpdatePerson)
	router.DELETE(PATH+"/person/:id", auth, inDB.DeletePerson)
	router.Run(":3001")
}

func MakeToken(user string) (int64, string, error) {
	claims := &jwt.StandardClaims{
		ExpiresAt: time.Now().Add(time.Hour * time.Duration(8)).Unix(),
		Issuer:    "FIM-API",
		Subject:   "USER",
		Audience:  user,
		NotBefore: time.Now().Unix()-1,
		IssuedAt:  time.Now().Unix(),
	}
	expired := time.Now().Add(time.Hour * time.Duration(8)).Unix()
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	tk,err := token.SignedString([]byte(jwtSecret))
	return expired,tk,err
}

func loginHandler(c *gin.Context) {
	var user Credential
	err := c.Bind(&user)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"status":  http.StatusBadRequest,
			"message": "can't bind struct",
		})
	}else{
		var (
			person structs.Person
		)
		queryStmt, err := db.Prepare("SELECT Username,Password,First_Name,Last_Name,Telp,Age FROM people WHERE username=$1")
		err = queryStmt.QueryRow(user.Username).Scan(&person.Username,&person.Password,&person.First_Name,&person.Last_Name,&person.Telp,&person.Age)

		if err!= nil {
			c.JSON(http.StatusUnauthorized, gin.H{
				"status":  http.StatusUnauthorized,
				"message": err.Error(),
			})
		} else {
			if user.Password != person.Password {
				c.JSON(http.StatusUnauthorized, gin.H{
					"status":  http.StatusUnauthorized,
					"message": "wrong username or password",
				})
			}else{
				expired,tokenString,err := MakeToken(user.Username)

				if err != nil {
					c.JSON(http.StatusInternalServerError, gin.H{
						"message": err.Error(),
					})
					c.Abort()
				}
				c.JSON(http.StatusOK, gin.H{
					"expired": expired,
					"token": tokenString,
				})
			}
		}
	}
}

func refreshToken(c *gin.Context) {
	reqToken := strings.TrimSpace(c.Request.Header.Get("Authorization"))
	if(reqToken == "") {
		result := gin.H{
				"message": "not authorized",
				"error":   "Authorization needed",
			}
		c.JSON(http.StatusUnauthorized, result)
		c.Abort()
	}else{
		splitToken := strings.Split(reqToken, "Bearer")
		tokenString := strings.TrimSpace(splitToken[1])

		token, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
			if jwt.GetSigningMethod("HS256") != token.Method {
				return nil, fmt.Errorf("Unexpected signing method: %v", token.Header["alg"])
			}
			splitToken = strings.Split(reqToken, "Bearer")

			return []byte(jwtSecret), nil
		})

		// if token.Valid && err == nil {
		if token != nil && err == nil {
			tc := token.Claims.(jwt.MapClaims)
			expired,tokenString,err := MakeToken(tc["iss"].(string));
			if err != nil {
				c.JSON(http.StatusInternalServerError, gin.H{
					"message": err.Error(),
				})
				c.Abort()
			}
			c.JSON(http.StatusOK, gin.H{
				"expired": expired,
				"token": tokenString,
			})
		} else {
			result := gin.H{
				"message": "not authorized",
				"error":   err.Error(),
			}
			c.JSON(http.StatusUnauthorized, result)
			c.Abort()
		}
	}
}

func viewToken(c *gin.Context) {
	reqToken := strings.TrimSpace(c.Request.Header.Get("Authorization"))
	if(reqToken == "") {
		result := gin.H{
				"message": "not authorized",
				"error":   "Authorization needed",
			}
		c.JSON(http.StatusUnauthorized, result)
		c.Abort()
	}else{
		splitToken := strings.Split(reqToken, "Bearer")
		tokenString := strings.TrimSpace(splitToken[1])

		token, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
			if jwt.GetSigningMethod("HS256") != token.Method {
				return nil, fmt.Errorf("Unexpected signing method: %v", token.Header["alg"])
			}
			splitToken = strings.Split(reqToken, "Bearer")

			return []byte(jwtSecret), nil
		})

		// if token.Valid && err == nil {
		if token != nil && err == nil {
			tc := token.Claims.(jwt.MapClaims)
			c.JSON(http.StatusOK, gin.H{
				"data": tc,
			})
		} else {
			result := gin.H{
				"message": "not authorized",
				"error":   err.Error(),
			}
			c.JSON(http.StatusUnauthorized, result)
			c.Abort()
		}
	}
}

func auth(c *gin.Context) {
	reqToken := strings.TrimSpace(c.Request.Header.Get("Authorization"))
	if(reqToken == "") {
		result := gin.H{
				"message": "not authorized",
				"error":   "Authorization needed",
			}
		c.JSON(http.StatusUnauthorized, result)
		c.Abort()
	}else{
		splitToken := strings.Split(reqToken, "Bearer")
		tokenString := strings.TrimSpace(splitToken[1])

		token, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
			if jwt.GetSigningMethod("HS256") != token.Method {
				return nil, fmt.Errorf("Unexpected signing method: %v", token.Header["alg"])
			}
			splitToken = strings.Split(reqToken, "Bearer")

			return []byte(jwtSecret), nil
		})

		// if token.Valid && err == nil {
		if token != nil && err == nil {

		} else {
			result := gin.H{
				"message": "not authorized",
				"error":   err.Error(),
			}
			c.JSON(http.StatusUnauthorized, result)
			c.Abort()
		}
	}
}
