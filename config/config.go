package config

 import (
            "fmt"
            "database/sql"
            _ "github.com/lib/pq"
    )

    const (
            host = "localhost"
            port = 5432
            user = "postgres"
            password = "qwerty"
            dbname = "userdb"
    )


// DBInit create connection to database
func DBInit() (*sql.DB) {

	 psqlInfo := fmt.Sprintf("host=%s port=%d user=%s password=%s dbname=%s " +
              "sslmode=disable", host, port, user, password, dbname)

            db, err := sql.Open("postgres", psqlInfo)

            if err != nil {
                    panic(err)
            }


            return db
}
